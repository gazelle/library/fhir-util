package net.ihe.gazelle.fhir.constants;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.EncodingEnum;
import net.ihe.gazelle.fhir.context.FhirContextProvider;

/**
 * <p>FhirParser class.</p>
 *
 * @author aberge
 * @version 1.0: 10/11/17
 */
public class FhirParserProvider {

    /**
     * Private default constructor for the class
     */
    private FhirParserProvider() {
        //empty
    }

    /** Constant <code>jsonParser</code> */
    public static final IParser jsonParser;
    /** Constant <code>xmlParser</code> */
    public static final IParser xmlParser;

    static {
        FhirContext context = FhirContextProvider.instance();
        jsonParser = context.newJsonParser();
        jsonParser.setPrettyPrint(true);
        xmlParser = context.newXmlParser();
        xmlParser.setPrettyPrint(true);
    }

    /**
     * <p>Getter for the field <code>jsonParser</code>.</p>
     *
     * @return a {@link ca.uhn.fhir.parser.IParser} object.
     */
    public static IParser getJsonParser() {
        return jsonParser;
    }

    /**
     * <p>Getter for the field <code>xmlParser</code>.</p>
     *
     * @return a {@link ca.uhn.fhir.parser.IParser} object.
     */
    public static IParser getXmlParser() {
        return xmlParser;
    }

    /**
     * <p>getParserForEncoding.</p>
     *
     * @param encodingEnum a {@link ca.uhn.fhir.rest.api.EncodingEnum} object.
     * @return a {@link ca.uhn.fhir.parser.IParser} object.
     */
    public static IParser getParserForEncoding(EncodingEnum encodingEnum) {
        if (EncodingEnum.JSON.equals(encodingEnum)) {
            return jsonParser;
        } else {
            return xmlParser;
        }
    }
}
