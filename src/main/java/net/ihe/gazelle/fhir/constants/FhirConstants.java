package net.ihe.gazelle.fhir.constants;

/**
 * <p>FhirValidatorName class.</p>
 *
 * @author aberge
 * @version 1.0: 06/11/17
 */
public class FhirConstants {

    /** Constant <code>PIXM_REQUEST_TYPE="Get Corresponding Identifiers"</code> */
    public static final String PIXM_REQUEST_TYPE = "Get Corresponding Identifiers";
    /** Constant <code>PIXM_RESPONSE="Parameters Resource"</code> */
    public static final String PIXM_RESPONSE = "Query Return Corresponding Identifiers";
    /** Constant <code>PDQM_REQUEST_TYPE="Query Patient Resource"</code> */
    public static final String PDQM_REQUEST_TYPE = "Query Patient Resource";
    /** Constant <code>PDQM_RESPONSE="Query Patient Resource Response"</code> */
    public static final String PDQM_RESPONSE = "Query Patient Resource Response";
    /** Constant <code>OPERATION_OUTCOME="Operation Outcome"</code> */
    public static final String OPERATION_OUTCOME = "Operation Outcome";
    /** Constant <code>PDQM_RETRIEVE_REQUEST_TYPE="Retrieve Patient Resource"</code> */
    public static final String PDQM_RETRIEVE_REQUEST_TYPE = "Retrieve Patient Resource";
    /** Constant <code>PDQM_RETRIEVE_RESPONSE="Retrieve Patient Resource Response"</code> */
    public static final String PDQM_RETRIEVE_RESPONSE = "Retrieve Patient Resource Response";
    /** Constant <code>PIXM_OPERATION="$ihe-pix"</code> */
    public static final String PIXM_OPERATION = "$ihe-pix";
    /** Constant <code>PARAM_TARGET_IDENTIFIER="targetIdentifier"</code> */
    public static final String PARAM_TARGET_IDENTIFIER = "targetIdentifier";
    /** Constant <code>PARAM_TARGET_ID="targetId"</code> */
    public static final String PARAM_TARGET_ID = "targetId";
    /** Constant <code>MOTHERS_MAIDEN_NAME_EXTENSION_URL="http://hl7.org/fhir/StructureDefinition"{trunked}</code> */
    public static final String MOTHERS_MAIDEN_NAME_EXTENSION_URL = "http://hl7.org/fhir/StructureDefinition/patient-mothersMaidenName";

    private FhirConstants(){

    }


}
