package net.ihe.gazelle.fhir.resources;

import ca.uhn.fhir.model.api.annotation.Child;
import ca.uhn.fhir.model.api.annotation.Description;
import ca.uhn.fhir.model.api.annotation.Extension;
import ca.uhn.fhir.model.api.annotation.ResourceDef;
import ca.uhn.fhir.model.api.annotation.SearchParamDefinition;
import ca.uhn.fhir.rest.gclient.StringClientParam;
import ca.uhn.fhir.util.ElementUtil;
import net.ihe.gazelle.fhir.constants.FhirConstants;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.Patient;

/**
 * Created by xfs on 30/06/16.
 *
 * @author aberge
 * @version $Id: $Id
 */

@ResourceDef(name="Patient", profile="http://ihe.net/fhir/StructureDefinition/IHE.PDQm.PatientResource")
public class PatientFhirIHE extends Patient {

    private static final long serialVersionUID = 1L;

    @Child(name = "mothersMaidenName")
    @Extension(url= FhirConstants.MOTHERS_MAIDEN_NAME_EXTENSION_URL,isModifier = false, definedLocally = false)
    @Description(shortDefinition = "Mother's Maiden name: Mother's maiden (unmarried) name, commonly collected to help verify patient identity")
    private HumanName mothersMaidenName;

    /** {@inheritDoc} */
    @Override
    public boolean isEmpty() {
        return super.isEmpty() && ElementUtil.isEmpty(mothersMaidenName);
    }

    /**
     * <p>Getter for the field <code>motherMaidenName</code>.</p>
     *
     * @return a {@link org.hl7.fhir.r4.model.HumanName} object.
     */
    public HumanName getMothersMaidenName() {
        if (mothersMaidenName == null) {
            mothersMaidenName = new HumanName();
        }
        return mothersMaidenName;
    }

    /**
     * <p>Setter for the field <code>motherMaidenName</code>.</p>
     *
     * @param motherMaidenName a {@link org.hl7.fhir.r4.model.HumanName} object.
     */
    public void setMothersMaidenName(HumanName motherMaidenName) {
        this.mothersMaidenName = motherMaidenName;
    }

    /** Constant <code>SP_MOTHERS_MAIDEN_NAME_FAMILY="mothersMaidenName.family"</code> */
    @SearchParamDefinition(
            name = "mothersMaidenName.family",
            path = "Patient.mothersMaidenName.family",
            description = "The family in a mother's maiden name",
            type = "string"
    )
    public static final String SP_MOTHERS_MAIDEN_NAME_FAMILY = "mothersMaidenName.family";
    /** Constant <code>MOTHERS_MAIDEN_NAME_FAMILY</code> */
    public static final StringClientParam MOTHERS_MAIDEN_NAME_FAMILY = new StringClientParam("mothersMaidenName.family");

    /** Constant <code>SP_MOTHERS_MAIDEN_NAME_GIVEN="mothersMaidenName.given"</code> */
    @SearchParamDefinition(
            name = "mothersMaidenName.given",
            path = "Patient.mothersMaidenName.given",
            description = "The given in a mother's maiden name",
            type = "string"
    )
    public static final String SP_MOTHERS_MAIDEN_NAME_GIVEN = "mothersMaidenName.given";
    /** Constant <code>MOTHERS_MAIDEN_NAME_GIVEN</code> */
    public static final StringClientParam MOTHERS_MAIDEN_NAME_GIVEN = new StringClientParam("mothersMaidenName.given");


}
