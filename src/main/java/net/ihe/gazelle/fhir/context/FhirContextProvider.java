package net.ihe.gazelle.fhir.context;

import ca.uhn.fhir.context.FhirContext;

/**
 * Created by xfs on 25/08/16.
 *
 * @author aberge
 * @version $Id: $Id
 */
public final class FhirContextProvider {

    /**
     * Private default constructor for the class
     */
    private FhirContextProvider() {
        //empty
    }

    /**
     * <p>instance.</p>
     *
     * @return a {@link ca.uhn.fhir.context.FhirContext} object.
     */
    public static synchronized FhirContext instance() {
        return FhirContext.forR4();
    }
}
